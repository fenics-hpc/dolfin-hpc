// Copyright (C) 2003-2005 Anders Logg.
// Licensed under the GNU LGPL Version 2.1.
//
// First added:  2003-03-13
// Last changed: 2005

#include <dolfin/log/LogManager.h>

using namespace dolfin;

// Initialise static data
Logger LogManager::log;
