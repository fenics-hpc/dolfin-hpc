#ifndef __DOLFIN_FEM_H
#define __DOLFIN_FEM_H

// DOLFIN fem interface

#include <dolfin/fem/assemble.h>
#include <dolfin/fem/DofMap.h>
#include <dolfin/fem/DofMapSet.h>
#include <dolfin/fem/SubSystem.h>
#include <dolfin/fem/BoundaryCondition.h>
#include <dolfin/fem/DirichletBC.h>
#include <dolfin/fem/PeriodicBC.h>
#include <dolfin/fem/Form.h>
#include <dolfin/fem/Assembler.h>

#endif
