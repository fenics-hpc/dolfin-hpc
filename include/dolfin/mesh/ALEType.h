// Copyright (C) 2008 Solveig Bruvoll and Anders Logg.
// Licensed under the GNU LGPL Version 2.1.
//
// First added:  2008-05-02
// Last changed: 2008-05-28

#ifndef __ALE_TYPE_H
#define __ALE_TYPE_H

namespace dolfin
{

  /// List of available methods for ALE mesh movement
  enum ALEType {lagrange, hermite, harmonic, elastic};

}

#endif
