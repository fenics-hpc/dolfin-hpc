
AC_PREREQ(2.59)
AC_INIT([dolfin],[0.8.4-hpc])
AC_DEFINE_UNQUOTED(DOLFIN_VERSION,["${PACKAGE_VERSION}"],[Version of DOLFIN])
AM_INIT_AUTOMAKE
AM_MAINTAINER_MODE
AC_LANG(C++)
AC_CONFIG_MACRO_DIR([m4])
LT_INIT([disable-shared])
PACKAGE_REQUIRES=""
AC_SUBST(PACKAGE_REQUIRES)



# Feature options.
AC_ARG_ENABLE(debug, AC_HELP_STRING([--enable-debug],
		     [Compile with debug symbols]),
		     [enable_debug=${enableval}],[enable_debug=no])

AC_ARG_ENABLE(function-cache, AC_HELP_STRING([--enable-function-cache],
			      [Compile with local data cache in functions]),
			      [enable_function_cache=${enableval}],
			      [enable_function_cache=no])

AC_ARG_ENABLE(optimize-p1, AC_HELP_STRING([--enable-optimize-p1],
			   [Compile with optimization for P1 elements]),
			   [enable_optimize_p1=${enableval}],
			   [enable_optimize_p1=no])

AC_ARG_ENABLE(progress-bar, AC_HELP_STRING([--disable-progress-bar],
					   [Compile without progress bar]),
			    	     	   [enable_progressbar=no],
					   [enable_progressbar=yes])
AC_ARG_ENABLE(mpi,
              AC_HELP_STRING([--enable-mpi],
	      [Compile with support for MPI]),
	      [enable_mpi=${enableval}], [enable_mpi=no])

AC_ARG_ENABLE(mpi-io, AC_HELP_STRING([--enable-mpi-io],
		     [Compile with support for MPI I/O]),
		     [enable_mpiio=${enableval}],[enable_mpiio=no])

AC_ARG_ENABLE(openmp,
              AC_HELP_STRING([--enable-openmp],
	      [Compile with support for OpenMP]),
	      [enable_openmp=${enableval}], [enable_openmp=no])

AC_ARG_ENABLE(boost-tr1, AC_HELP_STRING([--enable-boost-tr1],
			 [Compile with support for Boost TR1 headers]),
			 [enable_boosttr1=${enableval}],[enable_boostr1=no])

AC_ARG_ENABLE(quadrature, AC_HELP_STRING([--enable-quadrature],
		     [Enable various quadrature rules]),
		     [enable_quad=${enableval}],[enable_quad=no])

AC_ARG_ENABLE(ufl, AC_HELP_STRING([--enable-ufl],
		     [Enable UFL support using UFC version 2.1.1]),
		     [enable_ufl=${enableval}],[enable_ufl=no])

AC_ARG_ENABLE(python, AC_HELP_STRING([--enable-python], 
              [Install Python modules, requires a Python interpreter]),
              [enable_python=${enableval}],[enable_python=no])

# Optional packages
AC_ARG_WITH(xml, AC_HELP_STRING([--with-xml],
		 [Compile with support for reading/writing XML data]),
		 [with_xml=${withval}],[with_xml=yes])




AC_ARG_WITH(gts, AC_HELP_STRING([--with-gts], [Compile with support for GTS]),
		 [with_gts=${withval}],[with_gts=no])

AC_ARG_WITH(janpack, AC_HELP_STRING([--with-janpack], 
		     [Compile with support for JANPACK]),
		     [with_janpack=${withval}],[with_janpack=no])

AC_ARG_WITH(petsc, AC_HELP_STRING([--with-petsc], 
		   [Compile with support for PETSc linear algebra]),
		   [with_petsc=${withval}],[with_petsc=no])

AC_ARG_WITH(libgeom, AC_HELP_STRING([--with-libgeom], 
		     [Compile with support for LIBGEOM]),
		     [with_libgeom=${withval}],[with_libgeom=no])

AC_ARG_WITH(parmetis, AC_HELP_STRING([--with-parmetis], 
		     [Compile with support for ParMETIS]),
		     [with_parmetis=${withval}],[with_parmetis=no])

AC_ARG_WITH(zoltan, AC_HELP_STRING([--with-zoltan], 
		     [Compile with support for Zoltan]),
		     [with_zoltan=${withval}],[with_zoltan=no])


if test "x${enable_function_cache}" = xyes; then
   AC_DEFINE(ENABLE_FUNCTION_CACHE, [1], 
   [Compile with local data cache in functions])
fi

if test "x${enable_optimize_p1}" = xyes; then
   AC_DEFINE(ENABLE_P1_OPTIMIZATIONS, [1], 
   [Compile with optimization for P1 elements])
fi

if test "x${enable_progressbar}" != xyes; then
   AC_DEFINE(NO_PROGRESS_BAR, [1], 
   [Compile without progress bar])
fi

if test "x${enable_boosttr1}" = xyes; then
   AC_DEFINE(ENABLE_BOOST_TR1,[1],[Compile with support for Boost TR1 headers])
   need_boost=yes	
fi


# Checks for programs.
AC_PROG_CXX
AC_PROG_INSTALL
AC_PROG_MAKE_SET
AC_PROG_LIBTOOL
PKG_PROG_PKG_CONFIG
AX_COMPILER_VENDOR
if test "x${enable_mpi}" != xno; then
   AX_MPI	
   CXX="$MPICXX"
   LIBS="$MPILIBS $LIBS"   
   if test "x${ax_cv_cxx_compiler_vendor}" = xsgi; then
      LIBS="$LIBS -lmpi++"
   fi   

   if test "x${enable_mpiio}" != xno; then
      AC_DEFINE(ENABLE_MPIIO, 1, [Compile with support for MPI I/O])
   fi
fi



if test "x${enable_openmp}" != xno; then
   AX_OPENMP
   CXXFLAGS="$CXXFLAGS $OPENMP_CXXFLAGS"
else
   if test "x${ax_cv_cxx_compiler_vendor}" = xcray; then
      CXXFLAGS="-hnoomp $CXXFLAGS" 
   fi
fi



# Checks for header files.
AC_CHECK_HEADERS([float.h stdlib.h string.h])
AX_CXX_HEADER_TR1_UNORDERED_MAP
AX_CXX_HEADER_TR1_UNORDERED_SET

if test "x${enable_ufl}" == xno; then
   PKG_CHECK_MODULES([UFC],[ufc-1 = 1.1])
   CPPFLAGS="$CPPFLAGS $UFC_CFLAGS"
else
   PKG_CHECK_MODULES([UFC],[ufc-1 = 2.1.1])
   CPPFLAGS="$CPPFLAGS $UFC_CFLAGS -DUFC_BACKWARD_COMPATIBILITY"
   AC_DEFINE(ENABLE_UFL, 1, [Enable UFL support using UFC version 2.1.1])
fi


if test "x${need_boost}" = xyes; then
   AX_BOOST_BASE([1.34],,AC_MSG_ERROR([suitable Boost not found]))
   CPPFLAGS="$CPPFLAGS $BOOST_CPPFLAGS"
fi

if test "x${enable_boosttr1}" = xyes; then
   if test "x${ax_cv_cxx_compiler_vendor}" = xsun; then
      CXXFLAGS="-D_RWSTD_ALLOCATOR $CXXFLAGS"
   fi
fi


# Checks for libraries.
if test "x${with_xml}" != xno; then
   AM_PATH_XML2(,,AC_MSG_ERROR([libxml2 not found]))
   LIBS="$XML_LIBS $LIBS"
   AC_DEFINE(HAVE_XML, 1, [Compile with support for reading/writing XML data])
fi

AX_ZLIB

if test "${ZLIB_LIBS}"; then
   LIBS="$ZLIB_LIBS $LIBS"
   enable_zlib="yes"
else
   enable_zlib="no"
fi

AX_CRAY

if test "x${enable_mpi}" != xno; then

found_domain_decomp="no"

   # This assumes a modern CRAY, for example XT, XE or XC series systems
   if test "x${is_cray}" = xyes; then
      if test "x${with_parmetis}" != xno; then
         AX_CRAY_PARMETIS
	 if test "x${have_cray_parmetis}" = xyes; then
	    found_domain_decomp="yes"
	 else
	    with_parmetis="no"
	 fi
      fi

      if test "x${with_zoltan}" != xno; then
      	 if test "x${enable_optimize_p1}" != xno; then
	    AC_MSG_ERROR([Zoltan requires facet connectivity])
	 fi

         AX_CRAY_ZOLTAN
	 if test "x${have_cray_zoltan}" = xyes; then
	    found_domain_decomp="yes"
	 else
	    with_zoltan="no"
	 fi
      fi
   else
      if test "x${with_parmetis}" != xno; then
         AX_PARMETIS
         if test "${PARMETIS_LIBS}"; then     
            LIBS="$PARMETIS_LIBS $LIBS"
	    found_domain_decomp="yes"
	    with_parmetis="yes"
	 else
	    with_parmetis="no"
         fi
      fi
      
      if test "x${with_zoltan}" != xno; then
      	 if test "x${enable_optimize_p1}" != xno; then
	    AC_MSG_ERROR([Zoltan requires facet connectivity])
	 fi

         AX_ZOLTAN
         if test "${ZOLTAN_LIBS}"; then     
            LIBS="$ZOLTAN_LIBS $LIBS"
	    found_domain_decomp="yes"
	    with_zoltan="yes"
	 else
	    with_zoltan="no"
         fi
      fi
   fi

   if test "x${found_domain_decomp}" != xyes; then
      AC_MSG_ERROR([Required partitioning library not found])
   fi
fi

if test "x${with_gts}" = xyes; then
   PKG_CHECK_MODULES([gts], [gts])
   CPPFLAGS="$CPPFLAGS $gts_CFLAGS"
   LIBS="$LIBS $gts_LIBS"
   AC_DEFINE(HAVE_GTS,[1],[Have GTS library])
fi

found_la_backend="no"

if test "x${with_petsc}" != xno; then
   # This assumes a modern CRAY, for example XT, XE or XC series systems
   if test "x${is_cray}" = xyes; then
      AX_CRAY_PETSC
      if test "x${have_cray_petsc}" = xyes; then      
         found_la_backend="yes"
      	 with_petsc="yes"
      else
	 with_petsc="no" 
      fi
   else
      AX_PETSC
      if test "${PETSC_LDFLAGS}"; then
         found_la_backend="yes"
	 with_petsc="yes"
      else
	 with_petsc="no"
      fi
   fi
fi

if test "x${with_janpack}" != xno; then
   AX_JANPACK
   if test "${janpack_LIBS}"; then
     found_la_backend="yes"  
   elif test "${janpack_mpi_LIBS}"; then
     found_la_backend="yes"  
   else
     with_janpack="no"
   fi
fi


if test "x${found_la_backend}" != xyes; then
   AC_MSG_ERROR([Linear Algebra backend not found])
fi

if test "x${enable_debug}" != xno; then
   CXXFLAGS="-g -DDEBUG $CXXFLAGS"
fi



if test "x${enable_quad}" = xyes; then 
   # This assumes a modern CRAY, for example XT or XE series systems
   if test "x${ax_cv_cxx_compiler_vendor}" = xcray; then
      AX_CRAY_LIBSCI
      if test $have_cray_libsci = yes; then
      	 have_lapack=yes	
      else
	 have_lapack=no
      fi
   elif test "x${ax_cv_cxx_compiler_vendor}" = xsgi; then
      AC_CHECK_LIB(scs,dgesv_,[have_lapack=yes, LIBS="$LIBS -lscs"], 
                   have_lapack=no,)
   elif test "x${ax_cv_cxx_compiler_vendor}" = xsun; then 
      AC_CHECK_LIB(sunperf,dgesv_,[have_lapack=yes, LIBS="$LIBS -lsunperf"], 
                   [have_lapack=no],)
   else
      AC_CHECK_LIB(lapack,dgesv_,
                   [have_lapack=yes, LIBS="$LIBS -lblas -llapack"], 
      		   [have_lapack=no], [-lblas])
   fi 

   if test $have_lapack = no; then
      AC_CHECK_LIB(lapack,dgesv_,[have_lapack=yes, LIBS="$LIBS"], 
                   have_lapack=no,)
   fi    

  
   if test $have_lapack = yes; then
      AC_DEFINE(HAVE_F77_LAPACK, [1], [Have FORTRAN LAPACK])
   elif test $have_lapack = no; then
      AC_MSG_ERROR([LAPACK not found, needed for various quadrature rules])
   fi
fi

if test "x${with_libgeom}" = xyes; then
   PKG_CHECK_MODULES([libgeom], [libgeom])
   CPPFLAGS="$CPPFLAGS $libgeom_CFLAGS"
   LIBS="$LIBS $libgeom_LIBS"
   AC_DEFINE(HAVE_LIBGEOM,[1],[Have LIBGEOM library])
fi

# Checks for typedefs, structures, and compiler characteristics.
AC_C_BIGENDIAN
AC_C_INLINE
AC_TYPE_SIZE_T
AC_TYPE_UINT32_T
AC_TYPE_UINT8_T

if test "x${ac_cv_c_bigendian}" != xno; then
   AC_DEFINE(HAVE_BIG_ENDIAN, 1, [Big endian byte order.])
fi


# Checks for library functions.
AC_FUNC_ERROR_AT_LINE
AC_CHECK_FUNCS([floor memset pow sqrt])

# Checks for Python
if test "x${enable_python}" = xyes; then
    AM_PATH_PYTHON([2.6])
    if ! ${HAVE_PYTHON}; then
        AC_MSG_ERROR([Python interpreter not found, module cannot be installed.])
    fi
fi
# Add variable for installation of Python modules
AM_CONDITIONAL([ENABLE_PYTHON_MODULES], [test "x${enable_python}" = xyes])

AC_CONFIG_HEADERS([include/dolfin/config/dolfin_config.h])
AC_CONFIG_FILES([Makefile src/Makefile\
			  src/common/Makefile\
			  src/elements/Makefile\
			  src/fem/Makefile\
			  src/function/Makefile\
			  src/graph/Makefile\
			  src/io/Makefile\
			  src/la/Makefile\
			  src/log/Makefile\
			  src/main/Makefile\
			  src/math/Makefile\
			  src/mesh/Makefile\
			  src/nls/Makefile\
			  src/parameter/Makefile\
			  src/pde/Makefile\
			  src/quadrature/Makefile\
			  src/dolfin.pc
			  include/Makefile
			  include/dolfin/Makefile])

# Python
AC_CONFIG_FILES([site-packages/Makefile])

AC_OUTPUT

# Display help text
echo \
"
---------------------------------------------------------

Configuration of DOLFIN ${PACKAGE_VERSION} finished.

Prefix: '${prefix}'.
Compiler: ${CXX} ${CXXFLAGS} ${CPPFLAGS}
Libs: ${LD} ${LDFLAGS} ${LIBS}

Package features:

  MPI: ${enable_mpi} 
  MPI I/O: ${enable_mpiio}
  OpenMP: ${enable_openmp}
  PETSc: ${with_petsc}
  JANPACK: ${with_janpack}
  GTS: ${with_gts}
  LIBGEOM: ${with_libgeom}
  ParMETIS: ${with_parmetis}
  Zoltan: ${with_zoltan}
  Function cache: ${enable_function_cache}
  Optimize P1: ${enable_optimize_p1}
  Progress bar: ${enable_progressbar}
  Compressed VTK: ${enable_zlib}
  Quadrature: ${enable_quad}
  UFL support: ${enable_ufl}


---------------------------------------------------------
"


